package domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Medicamento {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String termo;
	
	@Column
	private String tarja;
	
	@Column
	private Integer quantidadeEstoque;

	@ManyToOne
	private Unidade unidade;
	
	public Medicamento() {
		// TODO Auto-generated constructor stub
	}
	
	public Medicamento(String termo, String tarja, Integer quantidadeEstoque, Unidade unidade) {
		super();
		this.termo = termo;
		this.tarja = tarja;
		this.quantidadeEstoque = quantidadeEstoque;
		this.unidade = unidade;
	}

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public String getTarja() {
		return tarja;
	}

	public void setTarja(String tarja) {
		this.tarja = tarja;
	}

	public Integer getQuantidadeEstoque() {
		return quantidadeEstoque;
	}

	public void setQuantidadeEstoque(Integer quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}

	public Long getId() {
		return id;
	}
	
	public Unidade getUnidade() {
		return unidade;
	}
	
	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}
}
