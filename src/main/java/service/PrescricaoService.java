package service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import domain.model.Prescricao;
import domain.persistence.PrescricaoDAO;
import domain.util.JPAUtil;

@Path("prescricoes")
public class PrescricaoService {

	private PrescricaoDAO dao = new PrescricaoDAO(JPAUtil.createEntityManager());

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllByUsuarioAndData(@QueryParam(value = "cpfUsuario") String cpfUsuario,
			@QueryParam(value = "ano") Integer year, @QueryParam(value = "mes") Integer month,
			@QueryParam(value = "dia") Integer day) {
		List<Prescricao> prescricoes;
		try {
			prescricoes = dao.getAll();
		} catch (Exception e) {
			// TODO: Tratar acesso negado e retornar status adequado
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
		}
		return Response.status(Response.Status.OK).entity(prescricoes).build();
	}
}
